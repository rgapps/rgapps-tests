package cucumber.utils

import java.util.Collections

import com.rgapps.page.utils.DateUtils
import cucumber.runner.TimeService
import cucumber.runtime.io.{MultiLoader, ResourceLoader}
import cucumber.runtime.scala.ScalaBackend
import cucumber.runtime.xstream.LocalizedXStreams
import cucumber.runtime.{RuntimeGlue, StepDefinition}
import gherkin.pickles.{Argument, PickleLocation, PickleStep}
import org.junit.Assert

import _root_.scala.collection.JavaConverters._

object ComposedSteps {
  val stepDefinitions: Seq[StepDefinition] = {
    val classLoader: ClassLoader = getClass.getClassLoader
    val resourceLoader: ResourceLoader = new MultiLoader(classLoader)
    val glue = new RuntimeGlue(new LocalizedXStreams(classLoader))
    val scalaBackend = new ScalaBackend(resourceLoader)
    scalaBackend.loadGlue(glue, List("com.rga.webpage.features").asJava)

    glue.removeScenarioScopedGlue()

    scalaBackend.getStepDefinitions
  }
  var level = 0

  def RunStep(stepWithParameters: String) {
    val stepDefinition = getStepDefinitionsByPattern(stepWithParameters)
    val arguments = stepDefinition.matchedArguments(
      new PickleStep(stepWithParameters,
        Collections.emptyList[Argument](),
        Collections.emptyList[PickleLocation]())).asScala.map(_.getVal)
    val location = stepDefinition.getLocation(false)

    level += 1
    println(f"${"\t" * level}-> $stepWithParameters\t\t# $location")
    val startTime = TimeService.SYSTEM.time()
    try {
      stepDefinition.execute("scala", arguments.asJava.toArray())
    } catch {
      case e: Throwable =>
        e.setStackTrace(e.getStackTrace.filter(e => e.toString.contains("com.rga.") || e.toString.contains("ComposedSteps")))
        throw e
    } finally {
      val stopTime = TimeService.SYSTEM.time()
      println(f"${"\t" * level}-> ${DateUtils.parseDuration(startTime, stopTime)}")
      level -= 1
    }
  }

  private def getStepDefinitionsByPattern(stepString: String) = {
    val definitions = stepDefinitions.filter(p => stepString.matches(p.getPattern))
    Assert.assertEquals(s"Step definition not found: $stepString", 1, definitions.size)
    definitions.head
  }
}
