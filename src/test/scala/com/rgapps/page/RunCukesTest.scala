package com.rgapps.page

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

@RunWith(classOf[Cucumber])
@CucumberOptions(
  plugin = Array("pretty", "junit:target/cucumber/result.xml", "json:target/cucumber/cucumber.json", "html:target/cucumber"),
  features = Array("src/test/resources/features"),
  monochrome = true,
  glue = Array("com.rga.webpage.features"),
  tags = Array("not @Manual and not @InProgress")
)
class RunCukesTest
