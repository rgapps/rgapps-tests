package com.rgapps.page.contexts

import com.rgapps.page.utils.browser.BrowserProperties

object BrowserContext {
  val browserProperties: BrowserProperties = Settings.browserProperties
}
