package com.rgapps.page.contexts

import com.rgapps.page.utils.browser.BrowserProperties
import com.typesafe.config.{Config, ConfigFactory}

private[contexts] object Settings {
  private val settings: Config = ConfigFactory.systemEnvironment().withFallback(ConfigFactory.load())

  val browserProperties: BrowserProperties = BrowserProperties(
    name = settings.getString("BROWSER_NAME"),
    driverPath = settings.getString("BROWSER_WEB_DRIVER_PATH"),
    headless = settings.getBoolean("BROWSER_IS_HEADLESS"))
  val domain: String = settings.getString("DOMAIN")
}
