package com.rgapps.page.features

import java.io.{File, FileOutputStream}
import java.time.LocalDate

import com.rgapps.page.contexts.UIContext
import cucumber.api.scala.{EN, ScalaDsl}
import org.scalatest.Matchers
import org.scalatest.concurrent.{Eventually, IntegrationPatience}

class screenshotTaker extends ScalaDsl with EN with Matchers with Eventually with IntegrationPatience {

  // Take screenshot of browsers before closing them in every scenario
  After() { scenario =>
    if (scenario.isFailed) {
      val screenshot = UIContext.page.screenshot
      scenario.embed(screenshot, "image/png")
      val url = UIContext.page.currentUrl

      scenario.write(s"URL: $url")

      val fileName = s"target/test-reports/cucumber/${LocalDate.now().toString}-${System.currentTimeMillis}-${scenario.getName}.png"
      new FileOutputStream(new File(fileName)).write(screenshot)
      println(s"taking screenshot in $url ($fileName)")
    }
    UIContext.close()
  }
}
