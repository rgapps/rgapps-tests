package com.rgapps.page

import com.rgapps.page.utils.FileUtils
import net.masterthought.cucumber.{Configuration, ReportBuilder}

object GenerateReport extends App {

  import java.io.File

  import scala.collection.JavaConverters._

  private val inputPath = System.getenv("REPORT_OUTPUT")
  private val outputReportPath = s"$inputPath-report"

  val projectName = "HERE - OLP Notebooks"
  val buildNumber = System.getenv("GO_PIPELINE_LABEL")
  val reportOutputDirectory = new File(outputReportPath)
  val inputFiles = FileUtils.getFilesWithExtensionInPath(inputPath, ".json").filter(!_.contains("trends.json"))
  val configuration = new Configuration(reportOutputDirectory, projectName)
  configuration.setRunWithJenkins(false)
  configuration.setBuildNumber(buildNumber)
  configuration.setTagsToExcludeFromChart("@(?!(Smoke|Regression|Integration)).*")
  configuration.setTrends(new File(inputPath, "trends.json"), 50)

  val reportBuilder = new ReportBuilder(inputFiles.asJava, configuration)
  val result = reportBuilder.generateReports
}