package com.rgapps.page.utils.browser

case class BrowserProperties(name: String, driverPath: String, headless: Boolean)
