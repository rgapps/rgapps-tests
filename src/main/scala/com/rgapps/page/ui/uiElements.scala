package com.rgapps.page.ui

import java.util.{Calendar, Date}

case class Label(text: String, color: String = "", backgroundColor: String = "")

case class Button(text: String, enabled: Boolean, color: String = "", background: String = "")

case class Modal(title: String, message: String)

case class Credentials(username: String, password: String)

case class Cookie(name: String, value: String = "", creationDate: Date = new Date) {
  val expireDate: Date = {
    val calendar = Calendar.getInstance()
    calendar.setTime(creationDate)
    calendar.add(Calendar.MINUTE, 50)
    calendar.getTime
  }
}