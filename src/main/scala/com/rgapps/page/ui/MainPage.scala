package com.rgapps.page.ui

import com.rgapps.page.utils.browser.Page
import org.openqa.selenium.WebDriver

case class MainPage(driver: WebDriver) extends Page(driver) {
  def nameLabel: Option[Label] =
    (get elementOrNone id("name-label"))
      .map(_.getText.trim)
      .map(Label(_))

  def logoTextLabel: Option[Label] = {
    (get elementOrNone id("logo-label"))
      .map(_.getText.trim)
      .map(Label(_))
  }

  def languageButton: Option[Button] = {
    (get elementOrNone id("language-button"))
      .map(_.getText.trim)
      .map(Button(_, enabled = true))
  }

  def languageButtons: List[Some[Button]] = {
    val clickeables = get elements xpath("//*[contains(@id,'-language-button')]")
    clickeables.map { clickeable =>
      val buttonText = clickeable.getText.trim
      Some(Button(buttonText, enabled = true))
    }
  }

  object hoverOn {
    def languageButton(): Unit = {
      move to id("language-button")
    }
  }

  object unHoverOn {
    def languageButton(): Unit = {
      move outOf id("language-button")
    }
  }

  object clickOn {
    def languageButton(): Unit = {
      sendClick to id("language-button")
    }
  }

}
