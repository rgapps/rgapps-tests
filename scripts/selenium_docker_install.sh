#!/bin/bash
set -eux

docker stop selenium || true && docker rm selenium || true

docker run -d -p 4444:4444 --name selenium -v `pwd`:`pwd` -w `pwd` -v /dev/shm:/dev/shm selenium/standalone-chrome

docker ps -a
