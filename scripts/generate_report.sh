#!/usr/bin/env bash
set -eux

# Change to Path
initialPath=$(pwd)
thisFileThis="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${thisFileThis}/../
echo "Current Path: $(pwd)"

# Generate Report
export REPORT_OUTPUT="target/test-reports/cucumber"
sbt run

# Go back to initial Path
cd ${initialPath}
echo "Current Path: $(pwd)"