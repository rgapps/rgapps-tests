name := "webpage-test"

version := "0.1.0"

scalaVersion := "2.12.11"
resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
addSbtPlugin("com.artima.supersafe" % "sbtplugin" % "1.1.3")

libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.0"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.11"
libraryDependencies += "io.spray" %% "spray-json" % "1.3.3"
libraryDependencies += "io.cucumber" % "cucumber-core" % "2.0.1" % Test
libraryDependencies += "io.cucumber" %% "cucumber-scala" % "2.0.1" % Test
libraryDependencies += "io.cucumber" % "cucumber-jvm" % "2.0.1" % Test artifacts Artifact("cucumber-jvm", `type` = "pom", extension = "pom")
libraryDependencies += "io.cucumber" % "cucumber-junit" % "2.3.1" % Test
libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "3.10.0"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5"
libraryDependencies += "com.typesafe" % "config" % "1.3.2"
libraryDependencies += "net.masterthought" % "cucumber-reporting" % "3.20.0"

enablePlugins(CucumberPlugin)

CucumberPlugin.glue := "com.rga.webpage.features"
CucumberPlugin.monochrome := true